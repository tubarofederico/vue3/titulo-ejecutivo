import { createApp } from "vue";
import { createPinia } from "pinia";
import App from "./App.vue";
import hcdListPlugin from "@hcd-bsas/list-documents-plugin";
import VueSweetalert2 from "vue-sweetalert2";
import router from "./router/index";

import "./assets/main.scss";
import "./assets/tooltip.scss"; // Esto es necesario dado que estando en intranet no podemos importar la libreria de bootstrap no modificado

const pinia = createPinia();

const app = createApp(App);

app.use(pinia);
app.use(hcdListPlugin, {
	base_url: import.meta.env.VITE_URL_BASE,
	sweetalert: VueSweetalert2,
});
app.use(router);
app.mount("#hcd_tituloejecutivo");
