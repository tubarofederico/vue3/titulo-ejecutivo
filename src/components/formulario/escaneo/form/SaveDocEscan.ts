import { defineComponent } from "vue";
import type { AxiosResponse } from "axios";
import HCDEscaner from "@hcd-bsas/escaner-client";
import http from "@/http";
import Swal from "sweetalert2/dist/sweetalert2.js";
import type { SweetAlertOptions } from "sweetalert2";
import { DocEscanTipo } from "@/entities/DocEscanTipo";
import { Titulo } from "@/entities/Titulo";
import { mapStores } from "pinia";
import { useTitulosStore } from "@/stores/titulos";
import shared from "@/shared.js";

const { urlApiEscan } = shared();

class FileInfo {
	data: string | null = null;
	extension: string | null = null;
}
class GrabaInfo {
	id_doc_escan: number | null = null;
	id_rel_tabla_reg: number | null = null;
}
class ParamsSubida {
	file_data: string | null = null;
	id_doc_escan: number | null = null;
}
/*
class Swal {
	icon: string = "success";
	title: string = "&Eacute;xito";
	html: string = "El documento fue escaneado correctamente.";
	showConfirmButton: boolean = false;
	timer: number | null = 500
}
*/

export const saveDocEscan = defineComponent({
	/*
	name: "SaveDocEscan",
	template: '<div></div>',
	
	components: {
		AutocompleteNombreApellido,
	},
	*/
	render() {
		return false;
	},
	props: {
		titulo: {
			type: Titulo,
			required: false,
			default: undefined,
		},
		doc_escan_tipo: {
			type: DocEscanTipo,
			required: false,
			default: undefined,
		},
	},
	data() {
		return {
			hcd_escaner: null as HCDEscaner | null,
			msg_error: null as string | null,
		};
	},
	computed: {
		...mapStores(useTitulosStore),
	},
	mounted() {
		//this.hcd_escaner = new HCDEscaner();
		this.hcd_escaner = new HCDEscaner({
			sandbox: true,
		});
		//http.defaults.baseURL = "http://localhost/admin/escaner";
	},
	methods: {
		async generarDocEscan() {
			const swal = {
				icon: "success",
				title: "&Eacute;xito",
				html: "El documento fue escaneado correctamente.",
				showConfirmButton: false,
				timer: 500 as number | undefined,
			} as SweetAlertOptions;
			const params_subida = new ParamsSubida();
			let graba_info = new GrabaInfo();
			this.msg_error = "";
			try {
				const file_info: FileInfo = await this.hcd_escaner.getPdf({
					title: this.doc_escan_tipo!.d_tipo_doc_escan,
					count: this.doc_escan_tipo!.cant_max_escan,
				});
				graba_info = await this.grabarDocEscan(file_info.extension);
				//const rta_subida: AxiosResponse<number> = await this.subirDocEscan(file_info.data!);
				params_subida.file_data = file_info.data;
				params_subida.id_doc_escan = graba_info.id_doc_escan;
				await this.subirDocEscan(params_subida);
				//this.titulo!.id_rel_tabla_reg = graba_info.id_rel_tabla_reg!;

				if (this.titulo!.id_rel_tabla_reg === null) {
					this.titulo!.id_rel_tabla_reg = graba_info.id_rel_tabla_reg;
					if (!this.titulosStore.updateTitulo(this.titulo!)) {
						swal.icon = "warning";
						swal.title = "Advertencia";
						swal.html =
							"El documento fue escaneado correctamente, pero hubo un error al actualizar el listado de t&iacutetulos. ";
						swal.html +=
							"Recargue la p&acute;gina y, si encuentra alg&uacute;na inconsistencia, avise al Administrador.";
						swal.showConfirmButton = true;
						swal.timer = undefined;
					}
				}

				//this.$swal.fire(this.swal);
			} catch (error: unknown) {
				swal.icon = "error";
				swal.title = "Error";
				swal.showConfirmButton = true;
				swal.timer = undefined;
				if (graba_info.id_doc_escan === null) {
					this.reportarError(error, "A");
				} else {
					this.msg_error +=
						"Hubo un error interno al intentar subir el archivo al servidor.";
					this.reportarError(error, "S");
					await this.revertirDocEscan(graba_info.id_doc_escan!);
				}
				swal.html = this.msg_error;
			}
			Swal.fire(swal);
		},
		async grabarDocEscan(file_extension: string | null) {
			const form_data = new FormData();
			form_data.append("accion", "A");
			form_data.append("prefijo_escan", this.doc_escan_tipo!.prefijo_escan!);
			form_data.append("arch_extension", file_extension!);
			form_data.append(
				"id_tipo_doc_escan",
				this.doc_escan_tipo!.id_tipo_doc_escan!.toString()
			);
			form_data.append("estado", "A");
			form_data.append("id_tabla", this.titulo!.id_tabla.toString());
			form_data.append("id_tabla_reg", this.titulo!.id_titulo_ejec.toString());
			const rta: AxiosResponse<GrabaInfo> = await http.post<GrabaInfo>(
				urlApiEscan + "grabarDocEscanExc",
				form_data
			);
			return rta.data;
		},
		subirDocEscan(params_subida: ParamsSubida): Promise<AxiosResponse<number>> {
			const file_name =
				this.doc_escan_tipo!.prefijo_escan! +
				String(params_subida.id_doc_escan) +
				this.doc_escan_tipo!.extension!;
			const form_data = new FormData();
			form_data.append("parametros[fileBase64]", params_subida.file_data!);
			form_data.append("parametros[file_name]", file_name);
			form_data.append("parametros[ruta]", this.doc_escan_tipo!.ruta!);
			return http.post<number>(urlApiEscan + "uploadFileExc", form_data);
		},
		async revertirDocEscan(id_doc_escan: number) {
			const form_data = new FormData();
			form_data.append("accion", "B");
			form_data.append("id_doc_escan", id_doc_escan.toString());
			try {
				await http.post<GrabaInfo>(
					urlApiEscan + "grabarDocEscanExc",
					form_data
				);
				this.msg_error +=
					"<br />" +
					"Grabación previa en la base de datos revertida exitosamente.";
				this.msg_error += "<br />" + "Por favor, avise al Administrador.";
			} catch (error: unknown) {
				this.msg_error +=
					"<br />" +
					"Problemas al pretender revertir la grabación previa en la base de datos." +
					"<br />";
				this.reportarError(error, "B");
			}
		},
		reportarError(error: any, accion: string) {
			const swal = {
				icon: "error",
				title: "Error",
				html: "",
			};
			if (error.response) {
				// Request made and server responded
				if (accion !== "S") {
					this.msg_error +=
						error.message +
						" (" +
						error.code +
						")." +
						"<br />" +
						error.response.data.substring(
							error.response.data.indexOf("<h2>") + 4,
							error.response.data.indexOf("</h2>")
						);
				}
				console.log("Error: request made and server responded");
				console.log(swal.html);
				console.log(error.response);
				//console.log(error.response.status);
				//console.log(error.response.headers);
				//console.log(error.response.data);
			} else if (error.request) {
				// The request was made but no response was received
				this.msg_error +=
					"Error desconocido. Por favor, avise al Administrador.";
				console.log("Error: the request was made but no response was received");
				console.log(error.request);
			} else {
				// Something happened in setting up the request that triggered an Error
				this.msg_error +=
					"Error desconocido. Por favor, avise al Administrador.";
				console.log(
					"Error: something happened in setting up the request that triggered an error"
				);
				console.log(error);
			}
			//this.$swal.fire(swal);
		},
	},
});
