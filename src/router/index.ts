import { createRouter, createWebHashHistory } from "vue-router";

// VISTAS
import InicioTitulo from "@/views/InicioTitulo.vue";
import FormularioCompleto from "@/views/FormularioCompleto.vue";

// RUTAS
const routes = [
	{ path: "/", name: "listado", component: InicioTitulo },
	{
		path: "/form/:id?",
		name: "formulario",
		component: FormularioCompleto,
	},
];

const router = createRouter({
	history: createWebHashHistory(),
	routes,
});

export default router;
