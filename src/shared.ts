import HCDLoading from "@hcd-bsas/loading";

export default () => {
	// CONSTANTES
	const urlApi = import.meta.env.VITE_URL_API;
	const urlApiEscan = import.meta.env.VITE_URL_API_ESCAN;

	// METODOS
	function getUrl(path: string, params: string[] = []): string {
		return (
			import.meta.env.VITE_URL_API +
			path +
			(params.length > 0 ? "/" + params.join("/") : "")
		);
	}

	function isEmpty(data: any): boolean {
		try {
			return (
				data == null ||
				typeof data === "undefined" ||
				(typeof data === "string" && data.trim() === "") ||
				(Array.isArray(data) && data.length == 0) ||
				(typeof data === "object" &&
					data &&
					Object.keys(data).length === 0 &&
					Object.getPrototypeOf(data) === Object.prototype)
			);
		} catch (error) {
			console.log(error);
		}
		return false;
	}

	function isNumber(value: string) {
		return /^\d*$/.test(value);
	}

	const loading = new HCDLoading();

	return {
		// Data
		urlApi,
		urlApiEscan,

		// Metodos
		getUrl,
		isEmpty,
		isNumber,

		// Objetos
		loading,
	};
};
