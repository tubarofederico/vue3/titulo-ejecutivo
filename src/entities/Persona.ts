export class Persona {
	cuit!: string;
	calle_legal: string;
	nro_legal: string;
	piso_legal: string;
	dto_legal: string;
	loc_legal: string;
	cp_legal: string;
	pcia_legal: string = "";
	nombres: string;
	apellido: string;
	nro_id_trib: string;
	tipo_doc: string;
	nro_doc: string;

	constructor(
		tipo_doc: string,
		nro_doc: string,
		calle_legal: string = "",
		nro_legal: string = "",
		piso_legal: string = "",
		dto_legal: string = "",
		loc_legal: string = "",
		cp_legal: string = "",
		pcia_legal: string = "",
		nombres: string = "",
		apellido: string = "",
		nro_id_trib: string = ""
	) {
		this.tipo_doc = tipo_doc;
		this.nro_doc = nro_doc;
		this.calle_legal = calle_legal;
		this.nro_legal = nro_legal;
		this.piso_legal = piso_legal;
		this.dto_legal = dto_legal;
		this.loc_legal = loc_legal;
		this.cp_legal = cp_legal;
		this.pcia_legal = pcia_legal;
		this.nombres = nombres;
		this.apellido = apellido;
		this.nro_id_trib = nro_id_trib;
	}

	get domicilio(): string {
		return (
			(this.calle_legal != "" && this.calle_legal != null
				? this.calle_legal.trim() + " "
				: "") +
			(this.nro_legal != "" && this.nro_legal != null
				? this.nro_legal.trim() + " "
				: "") +
			(this.piso_legal != "" && this.piso_legal != null
				? this.piso_legal.trim() + " "
				: "") +
			(this.dto_legal != "" && this.dto_legal != null
				? this.dto_legal.trim() + ", "
				: "") +
			(this.loc_legal != "" && this.loc_legal != null
				? this.loc_legal.trim() + " "
				: "") +
			(this.pcia_legal != "" && this.pcia_legal != null
				? this.pcia_legal.trim()
				: "")
		);
	}

	get ayn(): string {
		return this.nombres + " " + this.apellido;
	}
}
