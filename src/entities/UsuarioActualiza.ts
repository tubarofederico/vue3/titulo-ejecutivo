export class UsuarioActualiza {
	id_titulos_ejec_dep_actualiza: number;
	actualiza: string;
	id_usuario: string;

	constructor(
		id_titulos_ejec_dep_actualiza: number = 0,
		actualiza: string = "",
		id_usuario: string = ""
	) {
		this.id_titulos_ejec_dep_actualiza = id_titulos_ejec_dep_actualiza;
		this.actualiza = actualiza;
		this.id_usuario = id_usuario;
	}

	esLegales(): boolean {
		return this.actualiza == "L";
	}

	esDGA(): boolean {
		return this.actualiza == "D";
	}
}
