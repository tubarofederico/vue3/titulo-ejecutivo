export interface ExpedienteInterface {
	id_expediente: number | null;
	id_caratula: number;
	clave_exp: string | undefined;
	expediente: string | undefined;
	anios_exp: number | undefined;
	numero_exp: number | undefined;
	origen_exp: number | undefined;
	alcance: number | undefined;

	toString(): string;
}

export class Expediente implements ExpedienteInterface {
	id_expediente: number | null;
	id_caratula: number;
	clave_exp: string | undefined;
	expediente: string | undefined;
	anios_exp: number | undefined;
	numero_exp: number | undefined;
	origen_exp: number | undefined;
	alcance: number | undefined;

	constructor() {
		this.id_expediente = null;
		this.id_caratula = 0;
	}

	toString(): string {
		return (
			this.origen_exp +
			"-" +
			this.numero_exp +
			" / " +
			this.anios_exp +
			" " +
			this.alcance
		);
	}
}
