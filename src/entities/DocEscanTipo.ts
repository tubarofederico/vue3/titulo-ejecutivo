export class DocEscanTipo {
	id_tipo_doc_escan_num: number | undefined = undefined;
	d_tipo_doc_escan: string | null = "--Seleccione--";
	campo_escan: string | null = null;
	prefijo_escan: string | null = null;
	extension: string | null = null;
	ruta: string | null = null;
	cant_max_escan_num: number | null = null;

	get id_tipo_doc_escan(): number | undefined {
		return this.id_tipo_doc_escan_num;
	}
	set id_tipo_doc_escan(newValue: number | undefined) {
		this.id_tipo_doc_escan_num = Number(newValue);
	}
	get cant_max_escan(): number | null {
		return this.cant_max_escan_num;
	}
	set cant_max_escan(newValue: number | null) {
		this.cant_max_escan_num = Number(newValue);
	}

}
