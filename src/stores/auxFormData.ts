import { defineStore } from "pinia";
import http from "@/http";
import type { AxiosResponse } from "axios";
import shared from "@/shared.js";

// Entities
import { TipoDoc } from "@/entities/TipoDoc";
import type { Normativa } from "@/entities/Normativa";
import { UsuarioActualiza } from "@/entities/UsuarioActualiza";
import type { EstadoRef as EstadoRefType } from "@/entities/EstadoRef";

const { urlApi } = shared();

export const useAuxFormDataStore = defineStore("auxFormData", {
	state: () => ({
		tiposDoc: [] as TipoDoc[],
		normativaVigente: undefined as Normativa | undefined,
		usuarioActualiza: undefined as UsuarioActualiza | undefined,
		estadoTitulos: [] as EstadoRefType[],
	}),
	actions: {
		async getTiposDoc(): Promise<Boolean> {
			if (this.tiposDoc.length == 0) {
				try {
					const response: AxiosResponse<TipoDoc[]> = await http.request<
						TipoDoc[]
					>({
						method: "GET",
						url: urlApi + "tiposDoc",
						transformResponse: [
							(data: string) => {
								const tiposDoc: TipoDoc[] = JSON.parse(data);
								const dataAux: TipoDoc[] = [];
								tiposDoc.forEach((element: TipoDoc) => {
									dataAux.push(Object.assign(new TipoDoc(), element));
								});
								return dataAux;
							},
						],
					});
					this.tiposDoc = response.data;
					return true;
				} catch (error) {
					console.log(error);
				}
				return false;
			}
			return true;
		},
		async getNormativaVigente(): Promise<Boolean> {
			if (typeof this.normativaVigente === "undefined") {
				try {
					const response = await http.get(urlApi + "normativaVigente");
					if (response.data.status == "success") {
						this.normativaVigente = response.data.res;
						return true;
					}
				} catch (error) {
					console.log(error);
				}
			}
			return false;
		},
		async getUsuarioActualiza(): Promise<Boolean> {
			if (typeof this.usuarioActualiza === "undefined") {
				try {
					const usuarioResponse = await http.get(
						urlApi + "getUsuarioActualiza"
					);
					this.usuarioActualiza = new UsuarioActualiza();
					this.usuarioActualiza.id_titulos_ejec_dep_actualiza =
						usuarioResponse.data.id_titulos_ejec_dep_actualiza;
					this.usuarioActualiza.actualiza = usuarioResponse.data.actualiza;
					this.usuarioActualiza.id_usuario = usuarioResponse.data.id_usuario;
					return true;
				} catch (error) {
					throw new Error("Usuario inexistente");
				}
			}
			return false;
		},
		async getEstados(): Promise<Boolean> {
			if (this.estadoTitulos.length == 0) {
				try {
					const rta = await http.get(urlApi + "estados");
					if (rta.data.status == "success") {
						this.estadoTitulos = rta.data.res;
						return true;
					}
					return false;
				} catch (error) {
					console.log(error);
				}
				return false;
			}
			return true;
		},
	},
});
