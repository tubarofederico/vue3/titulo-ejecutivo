import { defineStore } from "pinia";
import http from "@/http";
import type { Persona } from "@/entities/Persona";
import { EstadoRef } from "@/entities/EstadoRef";
import { Titulo } from "@/entities/Titulo";
import type { AxiosResponse } from "axios";
import shared from "@/shared.js";

const { urlApi } = shared();

export const useTitulosStore = defineStore("titulos", {
	state: () => ({ listadoTitulos: [] as Titulo[] }),
	actions: {
		async updateListado(params: {
			persona?: Persona;
			estado: EstadoRef | null;
		}) {
			if (typeof params.estado === "undefined") {
				throw new Error("Debe especificar un estado");
			}

			const paramsRequest: any = {
				cuit: null,
				id_estado: null,
			};

			if (typeof params.persona !== "undefined") {
				paramsRequest.cuit = params.persona.cuit;
			}

			if (params.estado != null) {
				paramsRequest.id_estado = params.estado.id_estado;
			}

			const response: AxiosResponse<Titulo[]> = await http.request<Titulo[]>({
				method: "GET",
				url: urlApi + "busqueda",
				params: paramsRequest,
				transformResponse: [
					(data: string) => {
						const titulos: Titulo[] = JSON.parse(data);
						const dataAux: Titulo[] = [];
						titulos.forEach((element: Titulo) => {
							dataAux.push(Object.assign(new Titulo(), element));
						});
						return dataAux;
					},
				],
			});
			this.listadoTitulos = response.data;
		},
		async findTituloById(
			id: number,
			force: boolean = false
		): Promise<Titulo | undefined> {
			if (this.listadoTitulos.length == 0 || force) {
				await this.updateListado({
					persona: undefined,
					estado: new EstadoRef(),
				});
			}
			const titulo = this.listadoTitulos.find((titulo: Titulo) => {
				return titulo.id_titulo_ejec == id;
			});
			return new Promise((resolve) => {
				resolve(titulo);
			});
		},
		updateTitulo(titulo: Titulo) {
			const index: number = this.listadoTitulos.findIndex(
				(elem) => elem.id_titulo_ejec == titulo.id_titulo_ejec
			);
			if (index != -1) {
				this.listadoTitulos[index] = titulo;
				return true;
			}
			return false;
		},
		async cambiarEstadoTitulo(titulo: Titulo | undefined, estado: number) {
			if (typeof titulo === "undefined") {
				return false;
			}

			// const formData = new FormData();
			// formData.append("id_titulo_ejec", titulo.id_titulo_ejec.toString());
			// formData.append("estado", estado);
			const rta = await http.post(urlApi + "cambiarEstado", {
				id_titulo_ejec: titulo.id_titulo_ejec,
				estado: estado,
			});
			if (rta.data.status == "success") {
				return true;
			}
			return false;
		},
		ordenarListado(myObjKey: string, orden: string) {
			const myObjType: Titulo = this.listadoTitulos[0];
			if (myObjKey in myObjType) {
				this.listadoTitulos = this.listadoTitulos.sort((obj1, obj2) => {
					const dato1: any = obj1[myObjKey as keyof typeof myObjType];
					const dato2: any = obj2[myObjKey as keyof typeof myObjType];

					if (orden == "A") {
						// Ascendente
						return dato1 < dato2 ? -1 : 1;
					} else if (orden == "D") {
						// Descendente
						return dato1 > dato2 ? -1 : 1;
					}

					return 0;
				});
			}
		},
	},
});
